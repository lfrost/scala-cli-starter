# scala-cli-starter

This is a starter template for creating Scala command-line applications.  This includes an example implementation of command line parsing.  It then performs the common function of opening a text file specified as a commmand line option and looping through the lines in that file.  It also demonstrates how to read from stdin in a way that will work correctly both interactively and using I/O redirection.


## System Setup for Scala Development

There are only two things needed for a local Scala development environment: a Java 8+ SDK (either OpenJDK or Oracle Java will work) and sbt.  sbt is a build tool like Gradle and Maven.  It is available at https://www.scala-sbt.org/download.html.


## Building

From the main directory of this application, run `sbt`.  The first time will take some time because it will download several things including a Scala compiler.  At the sbt prompt, run the following commands.  If you have previously generated a coverage report, also run `coverageOff` and `clean` first.

`assembly`
Builds an executable fat jar (which can be run directly, without `java -jar`) at target/scala-2.13/scala-cli-starter-1.1.0.

`scalastyle`
Perform code style checks.

`test`
Runs all tests.

`run`
Runs the application in interactive mode.  Enter lines of text, which will be echoed to the console.  Enter the word done to exit the application.

`exit`
Exits sbt.

To generate a code coverage report, run (in the sbt shell) `clean`, then `coverageOn`, then `test`, and then `coverageReport`.  The last command will show the filesystem path for the HTML coverage report.


## Running

In addition to running the application inside of sbt, it can also be executed directly from the command line in three different ways.

1. To run in interactive mode, `target/scala-2.13/scala-cli-starter-1.1.0`
1. To run in batch mode, `target/scala-2.13/scala-cli-starter-1.1.0 -i data/input.txt`
1. To run in batch mode using I/O redirection, `target/scala-2.13/scala-cli-starter-1.1.0 < data/input.txt`


## Scala Documentation

* [sbt documentation](https://www.scala-sbt.org/documentation.html)
* [Scala Standard Library API](https://www.scala-lang.org/api/2.13.3/)
* [Scala style guidelines](https://docs.scala-lang.org/style/)
* [Scalastyle rules](http://www.scalastyle.org/rules-1.0.0.html)

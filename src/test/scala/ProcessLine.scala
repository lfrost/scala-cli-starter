/*
 * Copyright (c) 2017-2020 Lyle Frost <lfrost@cnz.com>.
 */

import main.{CliConfig, ProcessLine}
import org.scalatest.TryValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/** ProcessLine tests
 */
class ProcessLineSpec extends AnyFlatSpec with TryValues with Matchers {
    private val sentence      = "This is a random sentence."
    private val verboseString = "You entered: "

    "The sentence 'done'" should "be true" in {
        ProcessLine.done("done") should be (true)
    }

    "The sentence 'Done.'" should "be true" in {
        ProcessLine.done("Done.") should be (true)
    }

    "The sentence 'This is a random sentence.'" should "be false" in {
        ProcessLine.done(sentence) should be (false)
    }

    s"The sentence $sentence" should s"be '$sentence'" in {
        implicit val cc = CliConfig(verbose = false)
        val result = ProcessLine(sentence)
        result.isSuccess     should be (true)
        result.success.value should be (sentence)
    }

    s"The sentence $sentence" should s"be 'You entered: $verboseString$sentence'" in {
        implicit val cc = CliConfig(verbose = true)
        val result = ProcessLine(sentence)
        result.isSuccess     should be (true)
        result.success.value should be (s"$verboseString$sentence")
    }
}

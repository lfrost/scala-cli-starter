/*
 * Copyright (c) 2017-2020 Lyle Frost <lfrost@cnz.com>.
 */

package main

import scala.util.{Success, Try}

/** Singleton for processing a line of input.
 */
object ProcessLine {
    /** Process a line of input.
     *
     *  @param line Line to be parsed
     *  @param cc   CLI configuration
     *  @return     Text to display or an exception.
     */
    def apply(line:String)(implicit cc:CliConfig) : Try[String] = {
        // Add code here to do something with line.  Current behaviour
        // is to simply echo.
        if (cc.verbose) {
            Success(s"You entered: $line")
        } else {
            Success(line)
        }
    }

    /** Test for done command.
     *
     *  @param line A line of input
     *  @return     Is line a command to quit?
     */
    def done(line:String) : Boolean = {
        line.toLowerCase.trim.stripSuffix(".").trim == "done"
    }
}

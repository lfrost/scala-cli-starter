/*
 * Copyright (c) 2017-2020 Lyle Frost <lfrost@cnz.com>.
 */

package main

import java.io.{File, FileNotFoundException}
import java.nio.charset.{Charset, MalformedInputException, UnsupportedCharsetException}

import scala.io.{Codec, Source, StdIn}
import scala.util.{Failure, Success, Try}
import scala.util.control.NonFatal

/** CLI configuration
 *
 *  @param charset Character set
 *  @param infile  Input file
 *  @param verbose Verbose flag
 */
case class CliConfig(
    charset : String       = "utf-8",
    infile  : Option[File] = None,
    verbose : Boolean      = false
)

/** Starter template for Scala command-line applications.
 */
object Main extends App with ExitStatus {
    private val Pkg         = getClass.getPackage
    private val AppName     = Pkg.getImplementationTitle
    private val AppVersion  = Pkg.getImplementationVersion
    private val AppCommand  = AppName
    private val Prompt      = s"$AppName> "

    // CLI parser
    private val Parser = new scopt.OptionParser[CliConfig](AppCommand) {
        // Specify the application name and version for the help header.
        head(AppName, AppVersion)

        // Enable --help.
        help("help").
            text("prints this usage text")

        // Enable --version.
        version("version").
            text("display version then exit")

        opt[Unit]("verbose").
            text("verbose output").
            action((_, c) => c.copy(verbose = true))

        opt[String]("charset").
            text("character set (only applicable with --infile)").
            validate(x =>
                Try(new Codec(Charset forName x)) match {
                    case Success(_) => success
                    case Failure(e) => {
                        e match {
                            case _:UnsupportedCharsetException => failure(s"Invalid character set $x")
                            case NonFatal(e)                   => failure(s"Error processing charset $x: ${e.getMessage()}")
                        }
                    }
                }
            ).
            action((x, c) => c.copy(charset = x))

        opt[File]('i', "infile").
            text("input file").
            valueName("filename").
            validate(x =>
                if (x.exists) {
                    success
                } else {
                    failure(s"Option --infile: ${x.getPath()} does not exist")
                }
            ).
            action((x, c) => c.copy(infile = Some(x)))
    }

    // Parse the command line.
    Parser.parse(args, CliConfig()) match {
        case Some(cliConfig) => {
            implicit val cc = cliConfig
            implicit val codec = new Codec(Charset forName cliConfig.charset)

            Try {
                // Select input source based on the command line.
                val inputIterator = cliConfig.infile match {
                    case Some(infile) => {
                        // Read from the batch file specified on the command line.
                        val filename = infile.getAbsolutePath()
                        if (cliConfig.verbose) Console.println(s"Processing batch file $filename.")
                        Source.fromFile(filename).getLines()
                    }
                    case None         => {
                        // Default to stdin.  Mute prompt if input is not from the console.
                        val prompt = if (java.lang.System.console() == null) "" else Prompt
                        Iterator.continually(StdIn.readLine(prompt))
                    }
                }
                // Process each line of input.
                inputIterator.
                    takeWhile(line => line != null && !ProcessLine.done(line)).
                    foreach {
                        line => ProcessLine(line) match {
                            case Success(s) => {
                                Console.println(s)
                            }
                            case Failure(e) => {
                                Console.println(s"Processing input failed with exception ${e.getMessage()}.")
                                sys.exit(ExitFailure)
                            }
                        }
                    }
            } match {
                case Success(_) => ()  // sys.exit(ExitSuccess) here causes a "dead code" warning from the compiler.
                case Failure(e) => {
                    e match {
                        case _:FileNotFoundException   => Console.println("The input file was not found.")
                        case _:MalformedInputException => Console.println(s"Error reading input.  Likely ${cliConfig.charset} is not the correct character set.")
                        case NonFatal(e)               => Console.println(e)
                    }
                    sys.exit(ExitFailure)
                }
            }
        }
        case None            => {
            // Invalid command line.  Error message will have been displayed by scopt.
            sys.exit(ExitUsage)
        }
    }
}

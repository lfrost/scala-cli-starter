addSbtPlugin("com.eed3si9n"     % "sbt-assembly"          % "0.15.0")
addSbtPlugin("com.typesafe.sbt" % "sbt-git"               % "1.0.0")
addSbtPlugin("org.scalastyle"   % "scalastyle-sbt-plugin" % "1.0.0")
addSbtPlugin("org.scoverage"    % "sbt-scoverage"         % "1.6.1")
